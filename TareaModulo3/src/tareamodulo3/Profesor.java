package tareamodulo3;
public class Profesor {
    private String id;
    private String nombre;
    private String apellido;
    private int edad;
    private String fechaNac;
    private String profesion;

    public Profesor() {
    }

    public Profesor(String id, String nombre, String apellido, int edad, String fechaNac, String profesion) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.fechaNac = fechaNac;
        this.profesion = profesion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    
    public String _toString(){
    	return "Identidad: "+this.getId()
        +"\nNombre: "+this.getNombre()+" "+this.getApellido()
        +"\nEdad: "+this.getEdad()
                +"\nFecha Nacimiento: "+this.getFechaNac()
                +"\nProfesion u Oficio: "+this.getProfesion()+"\n";
    }
    
}
