package tareamodulo3;
public class Main {
    public static void main(String args[]){
        Profesor profe;
        Profesor profe2=new Profesor();
        
        //Hay dos maneras de poder llenar atributos de una clase
        //String id, String nombre, String apellido, int edad, String fechaNac, String profesion
        profe = new Profesor("0101","Arnol","Peralta",31,"12/02/1989","Ingeniero en Sistemas");
        
        profe2.setId("0103");
        profe2.setNombre("Gabriel");
        profe2.setApellido("Lopez");
        profe2.setFechaNac("13/12/1995");
        profe2.setEdad(24);
        profe2.setProfesion("Ingenieria industrial");
        
        //imprimir los profesores
        
                System.out.println("-***Primer Profesor***-"+
                	profe._toString()
        );
                System.out.println("-***Segundo Profesor***-"+
                	profe2._toString()
        );
    }
}
